# bitbucket-pipelines-php7.1-mysql

Docker image useful for bitbucket pipelines based on Debian Stretch with PHP 7.1 and MySQL 5.5.

## Packages installed

 - `apt-transport-https`, `lsb-release`, `ca-certificates`, `zip`, `unzip`, `git`, `wget`, `php7.1`, `php7.1-cli`, `php7.1-common`, `libapache2-mod-php7.1`, `php7.1-mysql`, `php7.1-fpm`, `php7.1-curl`, `php7.1-gd`, `php7.1-bz2`, `php7.1-mcrypt`, `php7.1-json`, `php7.1-tidy`, `php7.1-xml`, `php7.1-mbstring`, `php-redis`, `php-memcached`, `php-gettext`, `openssh-client`, `curl`, `software-properties-common`, `gettext`, `mysql-server`, `mysql-client`, `apt-transport-https`, `ruby`, `python`, `python3`, `perl`, `memcached`
 - [MySQL](https://www.mysql.com/) 5.5 (user `root:root`)
 - [PHP](http://www.php.net/) 7.1
 - Latest [Composer](https://getcomposer.org/)

## Bitbucket pipeline integration example

```YAML
image: susa4ostec/bitbucket-pipelines-php7.1-mysql
pipelines:
  default:
    - step:
        script:
          - service mysql start
          - mysql -h localhost --user=root --password=root -e "CREATE DATABASE test;"
          - composer install --no-interaction --no-progress --prefer-dist
```